FROM thyrlian/android-sdk:latest

RUN wget -O- https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y g++ make ruby ruby-dev jq nodejs zlib1g-dev libcurl4-openssl-dev graphicsmagick && \
    yes | sdkmanager "platform-tools" "platforms;android-27" "build-tools;27.0.3" && \
    gem install bundler